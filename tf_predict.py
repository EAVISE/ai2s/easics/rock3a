import cv2
import numpy as np
import argparse
import tensorflow as tf
from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2


def create_mnetv2():
    model = tf.keras.applications.mobilenet_v2.MobileNetV2(
        include_top=True,
        weights='imagenet',
        input_tensor=None,
        input_shape=None,
        pooling=None,
        classes=1000
    )
    return model


def create_resnet50():
    model = tf.keras.applications.resnet50.ResNet50(
        include_top=True,
        weights='imagenet',
        input_tensor=None,
        input_shape=None,
        pooling=None,
        classes=1000
    )
    return model


parser = argparse.ArgumentParser(description='predict')
parser.add_argument("input", help="Input sample for testing")
args = parser.parse_args()

model = create_mnetv2()
#model.summary()

img = cv2.imread(args.input)
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
img = np.expand_dims(img, 0)

if model.name == 'mobilenetv2_1.00_224':
    img = img / 255

pred = model.predict(img)

idx = np.argmax(pred[0])
print(idx, pred[0][idx])
