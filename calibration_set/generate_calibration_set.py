import os
import cv2
import glob
import random
import argparse


parser = argparse.ArgumentParser(description='Generate calibration set')
parser.add_argument("folder", help="Imagenet folder name")
args = parser.parse_args()

filenames = glob.glob(os.path.join(args.folder, "*", "*.JPEG"))
random.shuffle(filenames)

print("for")
for filename in filenames[:100]:
    img = cv2.imread(filename)
    img = cv2.resize(img, (224, 224))
    out_filename = os.path.basename(filename)
    cv2.imwrite(out_filename, img)
