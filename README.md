# Getting started

## Creating models

### Install RKNN compiler toolchain

Setup a new python3.8 virtual environment on development computer (tested with Ubuntu 22.04).
Activate the virtual environment and install the compiler toolkit:

```
git clone https://github.com/rockchip-linux/rknn-toolkit2.git
cd rknn-toolkit2/packages
pip install numpy
pip install rknn_toolkit2-1.5.2+b642f30c-cp38-cp38-linux_x86_64.whl
```

### Compile the models

In the same python virtual env run:

```
mkdir models
python create_model.py
```

This will generate the frozen tensorflow models `resnet50.pb` and `mnetv2.pb` in the `models` folder

To compile the frozen graphs to FP16 RKNN models do:

```
python compile_model.py models/resnet50.pb
python compile_model.py models/mnetv2.pb
```

To compile the frozen graphs to INT8 RKNN models do:

```
python compile_model.py models/resnet50.pb --cal_dataset calibration_set/dataset.txt
python compile_model.py models/mnetv2.pb --cal_dataset calibration_set/dataset.txt
```

## Install RockPi

### Install OS

Flash SD card (> 8GB) with the following image (tested): https://github.com/radxa-build/rock-3a/releases/download/20220801-0056/rock-3a-debian-bullseye-xfce4-arm64-20220801-0145-gpt.img.xz
Note that any Debian 11 image for RockPi 3A will likely work.

(balenaEtcher is a handy tool for this)

### Enable NPU

Connect to the board through ssh (rock@<ip address>, password = rock)

Edit `/boot/config.txt` to edit the device tree overlay.

Remove the following line to enable support for the NPU hardware:

```
dtoverlay=rk3568-disable-npu
```

Now run:

```
sudo /usr/local/sbin/update_extlinux.sh
```

and then reboot.

### Install RKNN runtime (toolkit 2 lite)

```
sudo apt-get update
sudo apt-get install -y python3-dev python3-pip gcc python3-opencv python3-numpy git
wget https://github.com/rockchip-linux/rknpu2/raw/master/runtime/RK356X/Linux/librknn_api/aarch64/librknnrt.so
sudo mv librknnrt.so /usr/lib/librknnrt.so
sudo ldconfig
git clone https://github.com/rockchip-linux/rknn-toolkit2.git
cd rknn-toolkit2/rknn_toolkit_lite2/packages/
pip3 install rknn_toolkit_lite2-1.5.2-cp39-cp39-linux_aarch64.whl
```

## Run the models on RockPi

Copy the `.rknn` model files from the `models` directory to the RockPi together with the `test.py` script and
a test image from the `calibration_set` folder (for example `n03930630_19624.JPEG`).

Then run:

```
python3 test.py resnet50_fp16.rknn n03930630_19624.JPEG
python3 test.py mnetv2_fp16.rknn n03930630_19624.JPEG
python3 test.py resnet50_int8.rknn n03930630_19624.JPEG
python3 test.py mnetv2_int8.rknn n03930630_19624.JPEG
```
