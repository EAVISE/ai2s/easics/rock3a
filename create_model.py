import tensorflow as tf
from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2


def create_mnetv2():
    model = tf.keras.applications.mobilenet_v2.MobileNetV2(
        include_top=True,
        weights='imagenet',
        input_tensor=None,
        input_shape=None,
        pooling=None,
        classes=1000
    )
    return model


def create_resnet50():
    model = tf.keras.applications.resnet50.ResNet50(
        include_top=True,
        weights='imagenet',
        input_tensor=None,
        input_shape=None,
        pooling=None,
        classes=1000
    )
    return model


def freeze_model(model, filename):
    # Convert Keras model to ConcreteFunction
    full_model = tf.function(lambda inputs: model(inputs))
    full_model = full_model.get_concrete_function([tf.TensorSpec(model_input.shape, model_input.dtype) for model_input in model.inputs])

    # Get frozen ConcreteFunction
    frozen_func = convert_variables_to_constants_v2(full_model)
    frozen_func.graph.as_graph_def()

    # Save frozen graph from frozen ConcreteFunction to hard drive
    tf.io.write_graph(graph_or_graph_def=frozen_func.graph,
                      logdir="./models",
                      name=filename,
                      as_text=False)


# save tf model as pb file
model = create_mnetv2()
model.summary()
freeze_model(model, 'mnetv2.pb')

model = create_resnet50()
model.summary()
freeze_model(model, 'resnet50.pb')

