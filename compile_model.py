import os
import cv2
import argparse
import numpy as np

from rknn.api import RKNN

INPUT_SIZE = 224

parser = argparse.ArgumentParser(description='Compile and quantize a tensorflow classifier model for RKNN inference')
parser.add_argument("model", help="Saved model from tensorflow (.pb file)")
parser.add_argument("--input", help="Input sample for testing")
parser.add_argument("--cal_dataset", help="Calibration dataset text file (.txt) containling a list of image files for quantization. If not provided, quantization is off", default=None)
args = parser.parse_args()

mean = 0
std = 255 if 'mnetv2' in args.model else 1
do_quantization = args.cal_dataset is not None
print(f"mean = {mean}, std = {std}")

# Create RKNN object
rknn = RKNN(verbose=True)

# Pre-process config
print('--> Config model')
rknn.config(mean_values=3*[mean],
            std_values=3*[std],
            target_platform='rk3568')
print('done')

print('--> Loading model')
ret = rknn.load_tensorflow(tf_pb=args.model,
                           inputs=['inputs'],
                           outputs=['Identity'],
                           input_size_list=[[1, INPUT_SIZE, INPUT_SIZE, 3]])
if ret != 0:
    print('Load model failed!')
    exit(ret)
print('done')

# Build Model
print('--> Building model')
ret = rknn.build(do_quantization=do_quantization,
                 dataset=args.cal_dataset)
if ret != 0:
    print('Build model failed!')
    exit(ret)
print('done')

# Export rknn model
print('--> Export rknn model')
ret = rknn.export_rknn(os.path.join(args.model.replace('.pb', '.rknn')))
if ret != 0:
    print('Export rknn model failed!')
    exit(ret)
print('done')

if args.input:
    # Set inputs
    orig_img = cv2.imread(args.input)
    img = cv2.cvtColor(orig_img, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, (INPUT_SIZE, INPUT_SIZE), interpolation=cv2.INTER_CUBIC)

    # Init runtime environment
    print('--> Init runtime environment')
    ret = rknn.init_runtime()
    if ret != 0:
        print('Init runtime environment failed!')
        exit(ret)
    print('done')

    # Inference
    print('--> Running model')
    outputs = rknn.inference(inputs=[img])
    print('done')

    idx = np.argmax(outputs[0][0])
    print(idx, outputs[0][0][idx])


rknn.release()
