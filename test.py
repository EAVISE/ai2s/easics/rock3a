import numpy as np
import argparse
import time
import cv2

from rknnlite.api import RKNNLite


parser = argparse.ArgumentParser(description='Run a model on rknn')
parser.add_argument("model", help="RKNN model file")
parser.add_argument("input", help="Input image")
args = parser.parse_args()

# Create RKNN object
rknn_lite = RKNNLite()

# load RKNN model
ret = rknn_lite.load_rknn(args.model)
if ret != 0:
    print('Load RKNN model failed')
    exit(ret)

ret = rknn_lite.init_runtime()
if ret != 0:
    print('Init runtime environment failed')
    exit(ret)

# Set inputs
orig_img = cv2.imread(args.input)
img = cv2.cvtColor(orig_img, cv2.COLOR_BGR2RGB)
img = cv2.resize(img, (224, 224), interpolation=cv2.INTER_CUBIC)

# Inference loop
for i in range(1000):
    start = time.time()
    outputs = rknn_lite.inference(inputs=[img])
    stop = time.time()

    idx = np.argmax(outputs[0][0])
    print(f"class = {idx}, score = {outputs[0][0][idx]:.3f}, time = {(stop - start) * 1000:.2f}ms")

rknn_lite.release()
